#include "QGVGraphPrivate.h"
#include <string>
#include <sstream>
#include <vector>
#include <QDebug>

QGVGraphPrivate::QGVGraphPrivate(Agraph_t *graph)
{
	setGraph(graph);
}

void QGVGraphPrivate::setGraph(Agraph_t *graph)
{
	_graph = graph;
}

void QGVGraphPrivate::checkNodes(int scrHeight, int scrWidth, int* monitors, int* borderX, int* borderY)
{
    //iegūst grafa punktu skaitu un attiecību, pret izmēru pikseļos
       std::vector<double> points = strToDouble(agget(_graph, "bb"), ',');
       double scale = double(scrHeight)/points.at(3);
       Agnode_t *v;
       qDebug() << "Scale: " << scale;

       //iterē cauri katrai grafa virsotnei
       for( v = agfstnode(_graph); v; v=agnxtnode(_graph,v)){
               qDebug() <<"\nNode: "<< agnameof(v);
               std::vector<double> position = strToDouble(agget(v, "pos"), ',');
               std::vector<double> width = strToDouble(agget(v, "width"), ',');
               std::vector<double> height = strToDouble(agget(v, "height"), ',');

               qDebug() << "Center coordinates: " << agget(v, "pos");
               //noskaidro vai virsotne krustojas ar kādu no X ass robežām
               for(int i=0; i < monitors[0]-1; i++){

                   double posMin = position.at(0)*scale - (width.at(0)*72/2*scale);
                   double posMax = position.at(0)*scale + (width.at(0)*72/2*scale);
                   if(i==0){
                       qDebug()<< "Node is between X coordinates: " << posMin << " - " << posMax;
                   }
                   //Maina koordinates, ja krustojas
                   if( posMax >= borderX[i] && posMin <= borderX[i]){
                       qDebug() << "This node is crossing vertical boundary! (" << borderX[i] <<")";
                       changeX(v, posMin, posMax, borderX[i], scale);
                       qDebug() << "New coordinates: " << agget(v, "pos");
                   }
               }
               //noskaidro vai virsotne krustojas ar kādu no Y ass robežām
               for(int i=0; i<monitors[1]-1; i++){

                   double posMin = position.at(1)*scale - (height.at(0)*72/2*scale);
                   double posMax = position.at(1)*scale + (height.at(0)*72/2*scale);
                   if(i==0){
                       qDebug()<< "Node is between Y coordinates: " << posMin << " - " << posMax;
                   }
                   //Maina koordinates, ja krustojas
                   if(posMax >= borderY[i] && posMin <= borderY[i]){
                       qDebug() << "This node is crossing horizontal boundary! (" << borderY[i] <<")";
                       changeY(v, posMin, posMax, borderY[i], scale);
                       qDebug() << "New coordinates: " << agget(v, "pos");
                   }
               }
           }

           return;
}

/*************************
Funkcija, kas mana grafa virsotnes X pozīciju
**************************/
void QGVGraphPrivate::changeX(Agnode_t* v, double posMin, double posMax, int borderX, double scale){

    std::vector<double> center = strToDouble(agget(v, "pos"), ',');
    qDebug() << "Changing X coordinate for node: " << agnameof(v) << endl;
    double newpos = center.at(0);
    double diff;
    std::string pos;
    int padding = 8; //attalums no robezas, lai virsotne precizi nesakristu ar ekrana malu

    if(abs(borderX-posMin)<abs(borderX-posMax)){
                diff = borderX-posMin+padding; //Bīdam virsotni pa labi no robežas
            } else {
                diff = borderX-posMax-padding; //Bīdam virsotni pa kreisi no robežas
            }

    newpos += diff/scale;

    std::ostringstream strs;
    strs << newpos;
    pos = strs.str();
    pos += ",";
    strs.clear();
    strs.str("");
    strs << center.at(1);
    pos += strs.str();

    agsafeset(v, "pos", const_cast<char*>(pos.c_str()), const_cast<char*>(pos.c_str()));
    return;

}
/*************************
Funkcija, kas mana grafa virsotnes Y pozīciju
**************************/
void QGVGraphPrivate::changeY(Agnode_t* v, double posMin, double posMax, int borderY, double scale){

std::vector<double> center = strToDouble(agget(v, "pos"), ',');

 qDebug() << "Changing Y coordinate for node: " << agnameof(v) << endl;
    double newpos = center.at(1);
    double diff;
    std::string pos;
    int padding = 8; //attalums no robezas, lai virsotne precizi nesakristu ar ekrana malu

    if(abs(borderY-posMin)<abs(borderY-posMax)){
                diff = borderY-posMin+padding; //Bīdam virsotni uz augšu no robežas
            } else {
                diff = borderY-posMax-padding; //Bīdam virsotni uz leju no robežas
            }

    newpos += diff/scale;

    std::ostringstream strs;
    strs << center.at(0);
    pos = strs.str();
    pos += ",";
    strs.clear();
    strs.str("");
    strs << newpos;
    pos += strs.str();

    agsafeset(v, "pos", const_cast<char*>(pos.c_str()), const_cast<char*>(pos.c_str()));
    return;
}

/********************
Funkcija, kas atgriež nolasītās simbolu virknes vērtības double vektorā
**************************/
std::vector<double> QGVGraphPrivate::strToDouble(std::string str, char c){
    std::vector<double> vect;
    std::string temp;
    size_t i = 0, start = 0, end;

    do {
        end = str.find_first_of ( c, start );
        temp = str.substr( start, end );
        if ( isdigit ( temp[0] ) )
        {
            vect.push_back ( atof ( temp.c_str ( ) ) );
            ++i;
        }
        start = end + 1;
    } while ( start );

    return vect;
}


Agraph_t* QGVGraphPrivate::graph() const
{
	return _graph;
}
