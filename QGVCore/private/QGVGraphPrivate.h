#ifndef QGVGRAPHPRIVATE_H
#define QGVGRAPHPRIVATE_H

#include <cgraph.h>
#include <vector>
#include <string>

class QGVGraphPrivate
{
	public:
		QGVGraphPrivate(Agraph_t *graph = NULL);

		void setGraph(Agraph_t *graph);
		Agraph_t* graph() const;
        void checkNodes(int height, int width, int* monitors, int* borderX, int* borderY);
        std::vector<double> strToDouble(std::string str, char c);
        void changeX(Agnode_t* v, double posMin, double posMax, int borderX, double scale);
        void changeY(Agnode_t* v, double posMin, double posMax, int borderX, double scale);

	private:
		Agraph_t* _graph;
};

#endif // QGVGRAPHPRIVATE_H
