/***************************************************************
QGVCore
Copyright (c) 2014, Bergont Nicolas, All rights reserved.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library.
***************************************************************/
#include "QGVScene.h"
// The following include allows the automoc to detect, that it must moc this class
#include "moc_QGVScene.cpp"
#include <vector>
#include <QDebug>

#include <QGVNode.h>
#include <QGVEdge.h>
#include <QGVSubGraph.h>

#include <QGVCore.h>
#include <QGVGraphPrivate.h>
#include <QGVGvcPrivate.h>
#include <QGVEdgePrivate.h>
#include <QGVNodePrivate.h>

QGVScene::QGVScene(const QString &name, QObject *parent, int screenX, int screenY) : QGraphicsScene(parent),
                                                           _label (NULL)
{
		_context = new QGVGvcPrivate(gvContext());
		_graph = new QGVGraphPrivate(agopen(name.toLocal8Bit().data(), Agdirected, NULL));
        this->screenX = screenX;
        this->screenY = screenY;
    //setGraphAttribute("fontname", QFont().family());
}

QGVScene::~QGVScene()
{
		gvFreeLayout(_context->context(), _graph->graph());
		agclose(_graph->graph());
		gvFreeContext(_context->context());
    delete _graph;
    delete _context;
}

void QGVScene::setGraphAttribute(const QString &name, const QString &value)
{
		agattr(_graph->graph(), AGRAPH, name.toLocal8Bit().data(), value.toLocal8Bit().data());
}

void QGVScene::setNodeAttribute(const QString &name, const QString &value)
{
		agattr(_graph->graph(), AGNODE, name.toLocal8Bit().data(), value.toLocal8Bit().data());
}

void QGVScene::setEdgeAttribute(const QString &name, const QString &value)
{
		agattr(_graph->graph(), AGEDGE, name.toLocal8Bit().data(), value.toLocal8Bit().data());
}

QGVNode *QGVScene::addNode(const QString &label)
{
		Agnode_t *node = agnode(_graph->graph(), NULL, TRUE);
    if(node == NULL)
    {
        qWarning()<<"Invalid node :"<<label;
        return 0;
    }
		QGVNode *item = new QGVNode(new QGVNodePrivate(node), this);
    item->setLabel(label);
    addItem(item);
    _nodes.append(item);
    return item;
}

QGVEdge *QGVScene::addEdge(QGVNode *source, QGVNode *target, const QString &label)
{
		Agedge_t* edge = agedge(_graph->graph(), source->_node->node(), target->_node->node(), NULL, TRUE);
    if(edge == NULL)
    {
        qWarning()<<"Invalid egde :"<<label;
        return 0;
    }

		QGVEdge *item = new QGVEdge(new QGVEdgePrivate(edge), this);
    item->setLabel(label);
    addItem(item);
    _edges.append(item);
    return item;
}

QGVSubGraph *QGVScene::addSubGraph(const QString &name, bool cluster)
{
    Agraph_t* sgraph;
    if(cluster)
				sgraph = agsubg(_graph->graph(), ("cluster_" + name).toLocal8Bit().data(), TRUE);
    else
				sgraph = agsubg(_graph->graph(), name.toLocal8Bit().data(), TRUE);

    if(sgraph == NULL)
    {
        qWarning()<<"Invalid subGraph :"<<name;
        return 0;
    }

		QGVSubGraph *item = new QGVSubGraph(new QGVGraphPrivate(sgraph), this);
    addItem(item);
    _subGraphs.append(item);
    return item;
}

void QGVScene::setRootNode(QGVNode *node)
{
    Q_ASSERT(_nodes.contains(node));
    char root[] = "root";
		agset(_graph->graph(), root, node->label().toLocal8Bit().data());
}

void QGVScene::setNodePositionAttribute()
{
  foreach(QGVNode* node, _nodes) {
    node->setAttribute("pos", node->posToAttributeString().toLocal8Bit().constData());
    node->setAttribute ("pin", "true");
  }
}

void QGVScene::loadLayout(const QString &text, bool optimize, int width, int height, int* monitors, int* borderX, int* borderY)
{
        int layoutStatus;
        _graph->setGraph(QGVCore::agmemread2(text.toLocal8Bit().constData()));
        if(optimize){
            _graph->checkNodes(height, width, monitors, borderX, borderY);
            //Mainitajam grafam iespejams kada skautne krustos virsotni, tapec pasaka, lai skautnes ietu loka gar virsotnem
            agsafeset(_graph->graph(), "splines", "splines", "splines");
            agsafeset(_graph->graph(), "splines", "true", "true");
            layoutStatus = gvLayout(_context->context(), _graph->graph(), "nop");
        }else{
            agsafeset(_graph->graph(), "splines", "splines", "splines");
            agsafeset(_graph->graph(), "splines", "true", "true");
            layoutStatus = gvLayout(_context->context(), _graph->graph(), "nop");
        }

        if(layoutStatus != 0)
    {
        qCritical()<<"Layout render error"<<agerrors()<<QString::fromLocal8Bit(aglasterr());
        return;
    }

    //Debug output
		//gvRenderFilename(_context->context(), _graph->graph(), "png", "debug.png");

    //Read nodes and edges
		for (Agnode_t* node = agfstnode(_graph->graph()); node != NULL; node = agnxtnode(_graph->graph(), node))
    {
            qDebug() << "Node " << agnameof(node) << " position: " << agget(node, "pos");
				QGVNode *inode = new QGVNode(new QGVNodePrivate(node), this);
                inode->setLabel(agnameof(node));
        inode->updateLayout();
        addItem(inode);
        _nodes.append (inode);
				for (Agedge_t* edge = agfstout(_graph->graph(), node); edge != NULL; edge = agnxtout(_graph->graph(), edge))
        {
						QGVEdge *iedge = new QGVEdge(new QGVEdgePrivate(edge), this);
            iedge->updateLayout();
            addItem(iedge);
            _edges.append (iedge);
        }

    }
    update();
}

void QGVScene::applyLayout(const QString &algorithm)
{
    if(gvLayout(_context->context(), _graph->graph(),
        algorithm.toLocal8Bit().data()) != 0)
    {
        /*
         * Si plantage ici :
         *  - Verifier que les dll sont dans le repertoire d'execution
         *  - Verifie que le fichier "configN" est dans le repertoire d'execution !
         */
        qCritical()<<"Layout render error"<<agerrors()<<QString::fromLocal8Bit(aglasterr());
        return;
    }

    //Debug output
		//gvRenderFilename(_context->context(), _graph->graph(), "canon", "debug.dot");
		//gvRenderFilename(_context->context(), _graph->graph(), "png", "debug.png");

    //Update items layout
    foreach(QGVNode* node, _nodes)
        node->updateLayout();

    foreach(QGVEdge* edge, _edges)
        edge->updateLayout();

    foreach(QGVSubGraph* sgraph, _subGraphs)
        sgraph->updateLayout();

    //Graph label
		textlabel_t *xlabel = GD_label(_graph->graph());
    if(xlabel)
    {
      if (_label == NULL)
        _label = addText(xlabel->text);
      else
        _label->setPlainText (xlabel->text);
      _label->setPos(QGVCore::centerToOrigin(QGVCore::toPoint(xlabel->pos, QGVCore::graphHeight(_graph->graph())), xlabel->dimen.x, -4));
    }

    update();
}

void QGVScene::render (const QString &algorithm) {
  gvRender(_context->context(), _graph->graph(),
      algorithm.toLocal8Bit().data(), NULL);
}

void QGVScene::render (const QString algorithm, const QString filename) {
  gvRenderFilename(_context->context(), _graph->graph(),
      algorithm.toLocal8Bit().data(),
      filename.toLocal8Bit().data());
}

void QGVScene::freeLayout() {
  gvFreeLayout(_context->context(), _graph->graph());
}

void QGVScene::clear()
{
    _nodes.clear();
    _edges.clear();
    _subGraphs.clear();
    QGraphicsScene::clear();
    _label = NULL;
}

#include <QGraphicsSceneContextMenuEvent>
void QGVScene::contextMenuEvent(QGraphicsSceneContextMenuEvent *contextMenuEvent)
{
    QGraphicsItem *item = itemAt(contextMenuEvent->scenePos(), QTransform());
    if(item)
    {
        item->setSelected(true);
        if(item->type() == QGVNode::Type)
            emit nodeContextMenu(qgraphicsitem_cast<QGVNode*>(item));
        else if(item->type() == QGVEdge::Type)
            emit edgeContextMenu(qgraphicsitem_cast<QGVEdge*>(item));
        else if(item->type() == QGVSubGraph::Type)
            emit subGraphContextMenu(qgraphicsitem_cast<QGVSubGraph*>(item));
        else
            emit graphContextMenuEvent();
    }
    QGraphicsScene::contextMenuEvent(contextMenuEvent);
}

void QGVScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    QGraphicsItem *item = itemAt(mouseEvent->scenePos(), QTransform());
    if(item)
    {
        if(item->type() == QGVNode::Type)
            emit nodeDoubleClick(qgraphicsitem_cast<QGVNode*>(item));
        else if(item->type() == QGVEdge::Type)
            emit edgeDoubleClick(qgraphicsitem_cast<QGVEdge*>(item));
        else if(item->type() == QGVSubGraph::Type)
            emit subGraphDoubleClick(qgraphicsitem_cast<QGVSubGraph*>(item));
    }
    QGraphicsScene::mouseDoubleClickEvent(mouseEvent);
}

void QGVScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    QGraphicsItem *item = itemAt(mouseEvent->scenePos(), QTransform());
    if(item)
    {
        if(item->type() == QGVNode::Type)
            emit nodeMouseRelease (qgraphicsitem_cast<QGVNode*>(item));
    }
    QGraphicsScene::mouseReleaseEvent(mouseEvent);
}

void QGVScene::checkNodes(int height, int width, int* monitors, int* borderX, int* borderY){
     qDebug() << agget(_graph->graph(), "bb");
    //iegūst grafa punktu skaitu un attiecību, pret izmēru pikseļos
       std::vector<double> points = strToDouble(agget(_graph->graph(), "bb"), ' ');
       double scale = double(height)/points.at(3);
       Agnode_t *v;

       //iterē cauri katrai grafa virsotnei
       foreach(QGVNode* node, _nodes){
           qDebug() << "\nNode: "<< node->label() <<endl;

           std::vector<double> position = strToDouble(node->getAttribute("pos"), ',');
           std::vector<double> width = strToDouble(node->getAttribute("width"), ',');
           std::vector<double> height = strToDouble(node->getAttribute("height"), ',');

           qDebug() << "Center coordinates : " << node->getAttribute("pos") << endl;
           qDebug() << "Width: " << node->getAttribute("width");
           //noskaidro vai virsotne krustojas ar kādu no X ass robežām
           for(int i=0; i < monitors[0]-1; i++){

               double posMin = position.at(0)*scale - (width.at(0)*72/2*scale);
               double posMax = position.at(0)*scale + (width.at(0)*72/2*scale);
               if(i==0){
                   qDebug() << "Node is between X coordinates: " << posMin << " - " << posMax << endl;
               }
               //Maina koordinates, ja krustojas
               if( posMax >= borderX[i] && posMin <= borderX[i]){
                   qDebug() << "This node is crossing vertical boundary! (" << borderX[i] <<")"<<endl;
                   changeX(node, posMin, posMax, borderX[i], scale);
                   qDebug() << "New coordinates: " << node->getAttribute("pos") << endl;
               }
           }
           //noskaidro vai virsotne krustojas ar kādu no Y ass robežām
           for(int i=0; i<monitors[1]-1; i++){

               double posMin = position.at(1)*scale - (height.at(0)*72/2*scale);
               double posMax = position.at(1)*scale + (height.at(0)*72/2*scale);
               if(i==0){
                   qDebug() << "Node is between Y coordinates: " << posMin << " - " << posMax << endl;
               }
               //Maina koordinates, ja krustojas
               if(posMax >= borderY[i] && posMin <= borderY[i]){
                   qDebug() << "This node is crossing horizontal boundary! (" << borderY[i] <<")"<<endl;
                   changeY(node, posMin, posMax, borderY[i], scale);
                   qDebug() << "New coordinates: " << node->getAttribute("pos") << endl;
               }
           }
           node->updateLayout();
        }

       foreach(QGVEdge* edge, _edges)
           edge->updateLayout();

   return;
}

/********************
Funkcija, kas atgriež nolasītās simbolu virknes vērtības double vektorā
**************************/
#include <string>
std::vector<double> QGVScene::strToDouble(QString string, char c){
    std::string str = string.toLocal8Bit().data();
    std::vector<double> vect;
    std::string temp;
    std::size_t i = 0, start = 0, end;

    do {
        end = str.find_first_of (c, start);
        temp = str.substr( start, end );
        if ( isdigit ( temp[0] ) )
        {
            vect.push_back ( atof ( temp.c_str ( ) ) );
            ++i;
        }
        start = end + 1;
    } while ( start );

    return vect;
}

#include <sstream>
/*************************
Funkcija, kas mana grafa virsotnes X pozīciju
**************************/
void QGVScene::changeX(QGVNode* node, double posMin, double posMax, int borderX, double scale){

    std::vector<double> center = strToDouble(node->getAttribute("pos"), ',');
    qDebug() << "Changing X coordinate for node: " << node->label() << endl;
    double newpos = center.at(0);
    double diff;
    std::string pos;
    int padding = 15; //attalums no robezas, lai virsotne precizi nesakristu ar ekrana malu

    if(abs(borderX-posMin)<abs(borderX-posMax)){
                diff = borderX-posMin+padding; //Bīdam virsotni pa labi no robežas
            } else {
                diff = borderX-posMax-padding; //Bīdam virsotni pa kreisi no robežas
            }

    newpos += diff/scale;

    std::ostringstream strs;
    strs << newpos;
    pos = strs.str();
    pos += ",";
    strs.clear();
    strs.str("");
    strs << center.at(1);
    pos += strs.str();

    node->setAttribute("pos", const_cast<char*>(pos.c_str()));
    return;

}
/*************************
Funkcija, kas mana grafa virsotnes Y pozīciju
**************************/
void QGVScene::changeY(QGVNode* node, double posMin, double posMax, int borderY, double scale){

std::vector<double> center = strToDouble(node->getAttribute("pos"), ',');

 qDebug() << "Changing Y coordinate for node: " << node->label() << endl;
    double newpos = center.at(1);
    double diff;
    std::string pos;
    int padding = 15; //attalums no robezas, lai virsotne precizi nesakristu ar ekrana malu

    if(abs(borderY-posMin)<abs(borderY-posMax)){
                diff = borderY-posMin+padding; //Bīdam virsotni uz augšu no robežas
            } else {
                diff = borderY-posMax-padding; //Bīdam virsotni uz leju no robežas
            }

    newpos += diff/scale;

    std::ostringstream strs;
    strs << center.at(0);
    pos = strs.str();
    pos += ",";
    strs.clear();
    strs.str("");
    strs << newpos;
    pos += strs.str();

    node->setAttribute("pos", const_cast<char*>(pos.c_str()));
    return;
}

#include <QVarLengthArray>
#include <QPainter>
void QGVScene::drawBackground(QPainter * painter, const QRectF & rect)
{
    const int gridSizeX = rect.width()/screenX;
    const int gridSizeY = rect.height()/screenY;

    const qreal left = int(rect.left());
    const qreal top = int(rect.top());

    QVarLengthArray<QLineF, 100> lines;

    for (qreal x = left; x < rect.right(); x += gridSizeX)
        lines.append(QLineF(x, rect.top(), x, rect.bottom()));
    for (qreal y = top; y < rect.bottom(); y += gridSizeY)
        lines.append(QLineF(rect.left(), y, rect.right(), y));

    painter->setRenderHint(QPainter::Antialiasing, false);

    painter->setPen(QColor(Qt::lightGray).lighter(110));
    //painter->drawLines(lines.data(), lines.size());
    painter->setPen(Qt::black);
    //painter->drawRect(sceneRect());
}
