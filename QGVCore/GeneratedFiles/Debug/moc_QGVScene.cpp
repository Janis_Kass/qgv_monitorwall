/****************************************************************************
** Meta object code from reading C++ file 'QGVScene.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../QGVScene.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QGVScene.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QGVScene_t {
    QByteArrayData data[17];
    char stringdata0[212];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QGVScene_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QGVScene_t qt_meta_stringdata_QGVScene = {
    {
QT_MOC_LITERAL(0, 0, 8), // "QGVScene"
QT_MOC_LITERAL(1, 9, 15), // "nodeContextMenu"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 8), // "QGVNode*"
QT_MOC_LITERAL(4, 35, 4), // "node"
QT_MOC_LITERAL(5, 40, 15), // "nodeDoubleClick"
QT_MOC_LITERAL(6, 56, 11), // "nodeChanged"
QT_MOC_LITERAL(7, 68, 16), // "nodeMouseRelease"
QT_MOC_LITERAL(8, 85, 15), // "edgeContextMenu"
QT_MOC_LITERAL(9, 101, 8), // "QGVEdge*"
QT_MOC_LITERAL(10, 110, 4), // "edge"
QT_MOC_LITERAL(11, 115, 15), // "edgeDoubleClick"
QT_MOC_LITERAL(12, 131, 19), // "subGraphContextMenu"
QT_MOC_LITERAL(13, 151, 12), // "QGVSubGraph*"
QT_MOC_LITERAL(14, 164, 5), // "graph"
QT_MOC_LITERAL(15, 170, 19), // "subGraphDoubleClick"
QT_MOC_LITERAL(16, 190, 21) // "graphContextMenuEvent"

    },
    "QGVScene\0nodeContextMenu\0\0QGVNode*\0"
    "node\0nodeDoubleClick\0nodeChanged\0"
    "nodeMouseRelease\0edgeContextMenu\0"
    "QGVEdge*\0edge\0edgeDoubleClick\0"
    "subGraphContextMenu\0QGVSubGraph*\0graph\0"
    "subGraphDoubleClick\0graphContextMenuEvent"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QGVScene[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x06 /* Public */,
       5,    1,   62,    2, 0x06 /* Public */,
       6,    1,   65,    2, 0x06 /* Public */,
       7,    1,   68,    2, 0x06 /* Public */,
       8,    1,   71,    2, 0x06 /* Public */,
      11,    1,   74,    2, 0x06 /* Public */,
      12,    1,   77,    2, 0x06 /* Public */,
      15,    1,   80,    2, 0x06 /* Public */,
      16,    0,   83,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void,

       0        // eod
};

void QGVScene::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QGVScene *_t = static_cast<QGVScene *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->nodeContextMenu((*reinterpret_cast< QGVNode*(*)>(_a[1]))); break;
        case 1: _t->nodeDoubleClick((*reinterpret_cast< QGVNode*(*)>(_a[1]))); break;
        case 2: _t->nodeChanged((*reinterpret_cast< QGVNode*(*)>(_a[1]))); break;
        case 3: _t->nodeMouseRelease((*reinterpret_cast< QGVNode*(*)>(_a[1]))); break;
        case 4: _t->edgeContextMenu((*reinterpret_cast< QGVEdge*(*)>(_a[1]))); break;
        case 5: _t->edgeDoubleClick((*reinterpret_cast< QGVEdge*(*)>(_a[1]))); break;
        case 6: _t->subGraphContextMenu((*reinterpret_cast< QGVSubGraph*(*)>(_a[1]))); break;
        case 7: _t->subGraphDoubleClick((*reinterpret_cast< QGVSubGraph*(*)>(_a[1]))); break;
        case 8: _t->graphContextMenuEvent(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QGVScene::*_t)(QGVNode * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGVScene::nodeContextMenu)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QGVScene::*_t)(QGVNode * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGVScene::nodeDoubleClick)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QGVScene::*_t)(QGVNode * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGVScene::nodeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QGVScene::*_t)(QGVNode * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGVScene::nodeMouseRelease)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QGVScene::*_t)(QGVEdge * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGVScene::edgeContextMenu)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QGVScene::*_t)(QGVEdge * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGVScene::edgeDoubleClick)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QGVScene::*_t)(QGVSubGraph * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGVScene::subGraphContextMenu)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QGVScene::*_t)(QGVSubGraph * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGVScene::subGraphDoubleClick)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QGVScene::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QGVScene::graphContextMenuEvent)) {
                *result = 8;
                return;
            }
        }
    }
}

const QMetaObject QGVScene::staticMetaObject = {
    { &QGraphicsScene::staticMetaObject, qt_meta_stringdata_QGVScene.data,
      qt_meta_data_QGVScene,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QGVScene::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QGVScene::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QGVScene.stringdata0))
        return static_cast<void*>(const_cast< QGVScene*>(this));
    return QGraphicsScene::qt_metacast(_clname);
}

int QGVScene::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGraphicsScene::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void QGVScene::nodeContextMenu(QGVNode * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QGVScene::nodeDoubleClick(QGVNode * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QGVScene::nodeChanged(QGVNode * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QGVScene::nodeMouseRelease(QGVNode * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QGVScene::edgeContextMenu(QGVEdge * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QGVScene::edgeDoubleClick(QGVEdge * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QGVScene::subGraphContextMenu(QGVSubGraph * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QGVScene::subGraphDoubleClick(QGVSubGraph * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QGVScene::graphContextMenuEvent()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
