#ifndef GRAPHWINDOW_H
#define GRAPHWINDOW_H
#include "QGVScene.h"
#include <QWidget>
#include <QEvent>


namespace Ui {
class GraphWindow;
}

class GraphWindow : public QWidget
{
    Q_OBJECT

public:
    explicit GraphWindow(QWidget *parent = 0);
    ~GraphWindow();
    void setSource(QString source, int screenX, int screenY, int width, int height, bool optimize);

private slots:
    void nodeContextMenu(QGVNode* node);
    void nodeDoubleClick(QGVNode* node);

protected:
    bool eventFilter(QObject *object, QEvent *event);

private:
    Ui::GraphWindow *ui;
    QGVScene *_scene;
};

#endif // GRAPHWINDOW_H
