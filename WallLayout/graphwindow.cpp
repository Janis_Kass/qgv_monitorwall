#include "graphwindow.h"
#include "ui_graphwindow.h"
#include "QGVScene.h"
#include "QGVNode.h"

#include <QKeyEvent>
#include <QDebug>
#include <QMenu>
#include <QMessageBox>
#include <QMainWindow>
#include <QPainter>

GraphWindow::GraphWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GraphWindow)
{
    ui->setupUi(this);
    qApp->installEventFilter(this);
}

GraphWindow::~GraphWindow()
{
    delete ui;
}

void GraphWindow::setSource(QString source, int screenX, int screenY, int width, int height, bool optimize = false)
{

    int monitors[] = {screenX, screenY};
    int borderX[monitors[0]-1]; //Monitoru robežas uz X ass
    int borderY[monitors[1]-1]; //Monitoru robežas uz Y ass

    _scene = new QGVScene("DEMO", this, screenX, screenY);

    //Aizpilda X ass robežu masīvu
    for(int i=1; i<monitors[0]; i++){
       borderX[i-1] = width/monitors[0]*i;
    }
    //Aizpilda Y ass robežu masīvu
    for(int i=1; i<monitors[1]; i++){
       borderY[i-1] = height/monitors[1]*i;
    }
    if(optimize){
        //_scene->checkNodes(height, width, monitors, borderX, borderY);
        _scene->loadLayout(source, true, width, height, monitors, borderX, borderY);
    } else {
        _scene->loadLayout(source, false,  width, height, monitors, borderX, borderY);
    }

    //_scene->setSceneRect(0, 0, width, height);

    connect(_scene, SIGNAL(nodeContextMenu(QGVNode*)), SLOT(nodeContextMenu(QGVNode*)));
    connect(_scene, SIGNAL(nodeDoubleClick(QGVNode*)), SLOT(nodeDoubleClick(QGVNode*)));
    ui->graphicsView->setScene(_scene);

    this->showFullScreen();

    //Fit in view
    ui->graphicsView->fitInView(_scene->sceneRect());
    qDebug() << _scene->sceneRect();

}

void GraphWindow::nodeContextMenu(QGVNode *node)
{
    //Context menu exemple
    QMenu menu(node->label());

    menu.addSeparator();
    menu.addAction(tr("Informations"));
    menu.addAction(tr("Options"));

    QAction *action = menu.exec(QCursor::pos());
    if(action == 0)
        return;
}

void GraphWindow::nodeDoubleClick(QGVNode *node)
{
    QMessageBox::information(this, tr("Node double clicked"), tr("Node %1").arg(node->getAttribute("pos")));
}

bool GraphWindow::eventFilter(QObject *object, QEvent *event)
{
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent *ke = static_cast<QKeyEvent *>(event);
        if (ke->key() == Qt::Key_Escape){
            this->close();
        }
    }
    return QObject::eventFilter(object, event);
}
