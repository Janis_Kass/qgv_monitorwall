#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileInfo>
#include "graphwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    char* readFile(FILE* fp);
    int height;
    int width;
    int screenX;
    int screenY;

private slots:
    void on_fileButton_clicked();
    void on_runButton_clicked();


    void on_runOptimizedButton_clicked();

private:
    Ui::MainWindow *ui;
    int dpiX;
    int screenCount;

    QString filename;
    QFileInfo fileinfo;
    GraphWindow *graphwindow;

};

#endif // MAINWINDOW_H
