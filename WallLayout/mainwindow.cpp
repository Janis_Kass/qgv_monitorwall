#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "graphwindow.h"
#include <QFileDialog>
#include <QFileInfo>
#include <QDesktopWidget>
#include <QDebug>
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    this->statusBar()->setSizeGripEnabled(false);

    QDesktopWidget *desktop = QApplication::desktop();
    QRect rec = desktop->screenGeometry();
    height = rec.height();
    width = rec.width();
    dpiX = desktop->logicalDpiX();
    screenCount = desktop->screenCount();

    //Pārbauda vai ekrānu skaits ir kvadrātisks. Ja ir, tad pieņem, ka ekrānu skaits rindā un kolonnā ir vienāds
    if(screenCount >=4 && double(floor(sqrt(screenCount))) == screenCount){
        screenX = screenY = sqrt(screenCount);
        ui->screensColumn->setText(QString::number(screenX));
        ui->screensRow->setText(QString::number(screenY));
    }
    ui->resolutionX->setText(QString::number(width));
    ui->resolutionY->setText(QString::number(height));

}

MainWindow::~MainWindow()
{
    graphwindow->close();
    delete ui;
}

void MainWindow::on_fileButton_clicked()
{
    QFileInfo info(QFileDialog::getOpenFileName(this, tr("Open graph file"), "", tr("Dot Files (*.dot)")));
    if(!info.baseName().isEmpty()){
        filename = info.absoluteFilePath();

    } else {
        filename = "";
    }
    ui->fileLabel->setText(info.baseName());
}

void MainWindow::on_runButton_clicked()
{

    if(filename.isEmpty()){
         ui->infoLabel->setText("Please provide a .dot file");
         return;
    }

    if(ui->screensColumn->text().isEmpty() || ui->screensRow->text().isEmpty() ){
        ui->infoLabel->setText("Enter screen count");
        return;
    }
    if(ui->resolutionX->text().isEmpty() || ui->resolutionY->text().isEmpty() ){
        ui->infoLabel->setText("Enter resolution");
        return;
    }

    ui->infoLabel->setText("");
    screenX = ui->screensRow->text().toInt();
    screenY = ui->screensColumn->text().toInt();

    width = ui->resolutionX->text().toInt();
    height = ui->resolutionY->text().toInt();

    QFileInfo info(filename);
    //Ģenerē xdot failu no dotā dot faila, izmantojot graphviz.
    string command = "dot -Tdot -Gsize=" + to_string(double(width)/96) + "," + to_string(double(height)/96) +"! -Gratio=" + to_string(double(height)/width) + " " + filename.toStdString().c_str() + " > " + info.baseName().toStdString().c_str() + "X.dot";
    system(command.c_str());

    //Atver uzģenerēto failu un ielasa to atmiņā.

    FILE* fp = fopen((info.baseName() + "X.dot").toStdString().c_str(), "r");
    char* source = readFile(fp);

    QString qstr = QString::fromLocal8Bit(source);

    graphwindow = new GraphWindow();
    graphwindow->show();
    graphwindow->setSource(qstr, screenX, screenY, width, height, false);

}

/*************************
Funkcija ielasa dot failu atmiņā
**************************/
char* MainWindow::readFile(FILE* fp){
    char *source = NULL;
    if(fp != NULL){
            if(fseek(fp, 0L, SEEK_END) == 0){
                long bufsize = ftell(fp);

            if (bufsize == -1) { /* Error */ }

            source = (char*)malloc(sizeof(char) * (bufsize + 1));

            if(fseek(fp, 0L, SEEK_SET) != 0) { /*Error*/}

            size_t newLen = fread(source, sizeof(char), bufsize, fp);
            if(newLen == 0){
                fputs("Error reading file", stderr);
            }else{
                source[newLen++] = '\0';
            }
        }
        fclose(fp);
    }
    return source;
}

void MainWindow::on_runOptimizedButton_clicked()
{
    if(filename.isEmpty()){
         ui->infoLabel->setText("Please provide a .dot file");
         return;
    }

    if(ui->screensColumn->text().isEmpty() || ui->screensRow->text().isEmpty() ){
        ui->infoLabel->setText("Enter screen count");
        return;
    }
    ui->infoLabel->setText("");

    screenX = ui->screensRow->text().toInt();
    screenY = ui->screensColumn->text().toInt();

    QFileInfo info(filename);
    //Ģenerē xdot failu no dotā dot faila, izmantojot graphviz.
    string command = "dot -Tdot -Gsize=" + to_string(double(width)/dpiX) + "," + to_string(double(height)/dpiX) +"! -Gratio="+ to_string(double(height)/width) +" "+ filename.toStdString().c_str() + " > " + info.baseName().toStdString().c_str() + "X.dot";
    system(command.c_str());

    //Atver uzģenerēto failu un ielasa to atmiņā.

    FILE* fp = fopen((info.baseName() + "X.dot").toStdString().c_str(), "r");
    char* source = readFile(fp);

    QString qstr = QString::fromLocal8Bit(source);

    graphwindow = new GraphWindow();
    graphwindow->show();
    graphwindow->setSource(qstr, screenX, screenY, width, height, true);
}
